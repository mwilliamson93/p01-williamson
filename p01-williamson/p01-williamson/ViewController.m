//
//  ViewController.m
//  p01-williamson
//
//  Edited file to give action to label
//
//  Created by Matthew Williamson on 1/27/16.
//  Copyright © 2016 Matthew Williamson. All rights reserved.
//

#import "ViewController.h"

int flip = 0;

@interface ViewController ()

@end

@implementation ViewController
@synthesize helloWorldLabel;
@synthesize eventButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clicked:(id)sender
{
    if (flip == 0) {
        [helloWorldLabel setText:@"Matthew Williamson"];
        flip = 1;
    }
    else {
        [helloWorldLabel setText:@"Style?"];
        flip = 0;
    }
    
    [eventButton setTitle:@"or a lot..." forState:UIControlStateNormal];
}

@end
